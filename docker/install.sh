#!/bin/bash

apt-get update
cd ~

# Install arduino-cli
apt-get install curl git python -y
curl -L -o arduino-cli.tar.bz2 https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2
tar xjf arduino-cli.tar.bz2
rm arduino-cli.tar.bz2
mv `ls -1` /usr/bin/arduino-cli

curl https://bootstrap.pypa.io/pip/2.7/get-pip.py -o get-pip.py
python get-pip.py
pip install pyserial

# Install esp32 core
printf "board_manager:\n  additional_urls:\n    - http://arduino.esp8266.com/stable/package_esp8266com_index.json\n" > .arduino-cli.yaml
arduino-cli core update-index --config-file .arduino-cli.yaml
arduino-cli core install esp8266:esp8266 --config-file .arduino-cli.yaml

# Install 'native' packages
arduino-cli lib install "Adafruit BME280 Library"
arduino-cli lib install "Adafruit Unified Sensor"
arduino-cli lib install "HCSR04 ultrasonic sensor"
arduino-cli lib install "ArduinoJson"
arduino-cli lib install "MPU9250_asukiaaa"
cd -

rm /usr/bin/arduino-cli
curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=/usr/bin sh -s 0.10.0

apt-get install python3 -y
